<?php 

// Implementation of preprocess_html(). 
// ==================================================================================
function cca_master_preprocess_html(&$variables) {
    if (!empty($variables['page']['mini_sidebar'])) {
        $variables['classes_array'][] = 'has-mini-sidebar';
    }

    if (!empty($variables['page']['sidebar'])) {
        $variables['classes_array'][] = 'has-sidebar';
    }
}




// Implementation of preprocess_page(). 
// ==================================================================================
function cca_master_preprocess_page(&$vars) {
    $vars['primary_local_tasks'] = menu_primary_local_tasks();
    $vars['secondary_local_tasks'] = menu_secondary_local_tasks();
}




// Implementation of user_links(). 
// ==================================================================================
function cca_master_user_links() {

    if ($_GET['q'] != 'user' && $_GET['q'] != 'user/login' && $_GET['q'] != 'user/register') {
        $destination = $_GET['q'];
    } else {
        $destination = $_GET['destination'];
    }

    if (user_is_logged_in()) {
        global $user;
        $output = l($user->name, 'users/'.$user->name, array('attributes' => array('class' => 'username')));
        if (is_numeric($user->picture)) {
            $output .= theme('user_picture', array('account' =>user_load($user->uid)));
        } else {
            $output .= l('<span class="default-user-picture"></span>', 'users/'.$user->name, array('html' => true));
        }
        
        $output .= '<ul class="links">';
        $output .= '<li class="first">'.l('account settings', 'user/'.$user->uid.'/edit', array('query' => array('destination' => $_GET['q']))).'</li>';
        $output .= '<li>'.l('log out', 'user/logout', array('query' => array('destination' => $destination))).'</li>';
        $output .= '</ul>';
        
    } else {
        $output = '<ul class="links">';
        $output .= '<li class="first">'.l('login', 'user/login', array('query' => array('destination' => $destination))).'</li>';
        $output .= '<li>'.l('register', 'user/register', array('query' => array('destination' => $destination))).'</li>';
        $output .= '</ul>';
    }
    
    return $output;
}
