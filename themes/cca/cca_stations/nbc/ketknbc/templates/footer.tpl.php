<div class="row" id="footer">
	<div class="span12 advert-leaderboard">
		<p>ADVERTISEMENT - Leaderboard</p>
	</div> <!-- /.advert-leaderboard -->
	
	<div class="span12 legal-menu">
		<?php if(menu_navigation_links('menu-legal-navigation')): ?>
			<?php print theme('links__menu-legal-navigation', array('links' => menu_navigation_links('menu-legal-navigation'), 'attributes' => array('class' => array('links', 'legal-menu')))); ?>
		<?php endif; ?>		
	</div> <!-- /.legal-menu -->
	
	<div>
		<div class="span2 footer-address">
			<h6><?=variable_get('station_name', '')?></h6>
			<p><?=variable_get('station_street_address', '')?> <br/>
			    <?=variable_get('station_city', '')?>, <?=variable_get('station_state', '')?> <?=variable_get('station_zip', '')?> <br/>
			    <?php if (variable_get('station_telephone', '')) : ?>
			        <strong>TEL:</strong> <?=variable_get('station_telephone', '')?><br/>
			    <?php endif; ?>
			    <?php if (variable_get('station_fax', '')) : ?>
			        <strong>FAX:</strong> <?=variable_get('station_fax', '')?><br/>
			    <?php endif; ?>
			    <?php if (variable_get('site_mail', '')) : ?>
			        <a href="mailto:<?=variable_get('site_mail', '')?>"><?=variable_get('site_mail', '')?></a></p>
			    <?php endif; ?>			
		</div> <!-- /.footer-address -->
		
		<div class="span6 footer-nav">
			
		</div> <!-- /.footer-nav -->
		
		<div class="span4 footer-social">
			
		</div> <!-- /.footer-social -->
	</div>
	
	<div class="span12 copyright-bar">		
		<?php if (menu_navigation_links('menu-station-information')) : ?>
			<?php print theme('links__menu-station-information', array('links' => menu_navigation_links('menu-station-information'), 'attributes' => array('class' => array('links', 'station-info-menu')))); ?>
		<?php endif; ?>
        
		<p>Copyright &copy; <?=date('Y')?> <?=variable_get('copyright_notice', '')?> &nbsp;|&nbsp; All Rights Reserved.</p>
	</div> <!-- /.copyright-bar -->
</div>

<!-- End of file footer.tpl.php || Location: ./cca_stations/nbc/ketknbc/templates/footer.tpl.php -->