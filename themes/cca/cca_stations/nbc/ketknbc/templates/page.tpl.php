<div class="container">

	<?php include('header.tpl.php'); ?>
	
	<div class="row" id="page-content">
		<div class="span8">
			<?php print render($page['content']); ?>
		</div>
		<div class="span4">Sidebar</div>
	</div>

	<?php include('footer.tpl.php'); ?>

</div>