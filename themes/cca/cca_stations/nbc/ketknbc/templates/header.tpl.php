<div class="row" id="header">
	<div class="top-bar">
		<div class="span8 legal-nav">KETK</div> <!-- /.legal-nav -->
		<div class="span4 search">darkgray</div> <!-- /.search -->
	</div> <!-- /.top-bar -->
	
	<div>
		<div class="span2 branding hidden-phone">tablet/desktop branding</div> <!-- /.branding-desktop -->
		<div class="span2 branding visible-phone">phone branding</div> <!-- /.branding-phone -->
		
		<div class="span10">
			<div class="row">
				<div class="span6 promo-banner">
					<div class="visible-phone">PHONE</div>
					<div class="visible-tablet">TABLET</div>
					<div class="visible-desktop">DESKTOP</div>						
				</div> <!-- /.promo-banner -->
				<div class="span4 social-spot">Connect/User</div> <!-- /.social-spot -->
			</div>
		
			<div class="row">
				<div class="span10 main-nav">Navigation</div> <!-- /.main-nav -->
			</div>
		</div>
	</div>
</div> <!-- /#header -->

<!-- End of file header.tpl.php || Location: ./cca_stations/nbc/ketknbc/templates/header.tpl.php -->