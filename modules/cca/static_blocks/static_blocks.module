<?php
// static_blocks.module
// Chris Brantley

function static_blocks_menu() {

    $items = array();

    $items['admin/config/content/static_blocks'] = array(
        'title' => 'Static Blocks',
        'description' => 'Configure template paths for static blocks',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('static_blocks_settings_form'),
        'access arguments' => array('administer static blocks'),
    );

    return $items;
}

function static_blocks_block_info() {

    $blocks = array();

    $static_blocks = static_blocks_get_blocks();

    foreach ($static_blocks as $name => $template) {
        $blocks[$name] = array(
            'info' => $name,
            'cache' => DRUPAL_CACHE_GLOBAL,
        );
    }

    return $blocks;

}

function static_blocks_block_view($delta = '') {

    $static_blocks = static_blocks_get_blocks();

    $block['subject'] = null;
    $block['content'] = theme_render_template($static_blocks[$delta], array());

    return $block;

}

function static_blocks_permission() {
    return array(
        'administer static blocks' => array(
            'title' => t('Administer Static Blocks Settings'), 
            'description' => t('Set the paths for static_blocks templates'),
        ),
    );
}

/**
 *
 * Static Block settings form
 *
 */
function static_blocks_settings_form($form, $form_state) {
    $form['static_blocks_paths'] = array(
        '#type' => 'textarea',
        '#title' => 'Template Paths',
        '#description' => 'The Static Blocks module will search within these paths for templates to be turned into blocks. If templates with the same name are found then the last one overrides the rest.',
        '#default_value' => variable_get('static_blocks_paths', 'sites/all/static_blocks'),
    );

    return system_settings_form($form);
}

/**
 *
 * Searches through the supplied paths for templates and returns an array
 *
 */
function static_blocks_get_blocks() {

    $static_blocks_paths = variable_get('static_blocks_paths', 'sites/all/static_blocks');
    $paths = explode("\n", $static_blocks_paths);

    $static_blocks = array();

    foreach ($paths as $path) {

        // Trim whitespace to get rid of spaces and carriage returns
        $path = trim($path);

        $mask = '/\.tpl\.php$/';
        $files = file_scan_directory($path, $mask, array('recurse' => false));

        foreach($files as $file) {
            $parts = explode('.', $file->filename);
            $file_name = array_shift($parts);
            $delta = str_replace('_', '/', $file_name);
            $static_blocks[$delta] = $path.'/'.$file->filename;
        }

    }

    return $static_blocks;

}
