<h3>Effective Date: Friday, June 11, 2010</h3>

<p>Thank you for visiting the <?=$station_name?> website (the "Site"). These Terms of Service are terms and 
    conditions that apply to your use of the Site.  By using this Site, you agree to be bound by these Terms 
    of Service and to use the Site in accordance with these Terms of Service, our Privacy Policy and any 
    additional terms and conditions referenced on our Site.</p>
<p>We reserve the right, from time to time, to modify these Terms of Service or to impose new conditions on 
    your use of the Site.  If these Terms of Service are modified, we will post the revised Terms of Service 
    on this Site and update the Effective Date to reflect the date of the changes.  By continuing to use the 
    Site after we post any such changes, you accept the Terms of Service as modified.</p>
<p>We further reserve the right to deny access to the Site, or any services provided via the Site, to anyone 
    who violates these Terms of Service or who, in our sole judgment, interferes with the ability of others 
    to enjoy this Site, or infringes upon the rights of others.</p>

<!-- SECTION -->
<h3>Privacy</h3>
<p>We respect the privacy rights of the users of our Site.  Please take a moment to review the terms of 
    our <a href="/privacy-policy">Privacy Policy</a>.</p>

<!-- SECTION -->
<h3>Site Content</h3>
<p>Your Rights to Use Site Content.  This Site and all the materials available on the Site are our property or 
    the property of our affiliates or licensors, and are protected by copyright, trademark, and other intellectual 
    property laws. The Site is provided solely for your personal non-commercial use. You may not use the Site or 
    the materials available on the Site in a manner that constitutes an infringement of our rights or that has 
    not been authorized by us.  Unless explicitly authorized in these Terms of Service or by the owner of the 
    materials, you may not modify, copy, reproduce, republish, upload, post, transmit, translate, sell, create 
    derivative works, exploit, or distribute in any manner or medium (including via email or other electronic 
    means) any material from the Site. You are permitted, however, to download and/or print one copy of individual 
    pages of the Site for your personal, non-commercial use, provided that you keep all copyright and other 
    proprietary notices.</p>
<p>Our Rights to Use Content You Submit. When you submit or post any material (including any photos or videos) 
    through our Site, you grant us, and anyone authorized by us, a royalty-free, perpetual, irrevocable, non-exclusive, 
    unrestricted, worldwide license to use, copy, modify, transmit, sell, exploit, create derivative works from, 
    distribute, and/or publicly perform or display such material, in whole or in part, in any manner or medium 
    (whether now known or hereafter developed), for any purpose that we choose, including, but not limited to, 
    exploiting proprietary rights under copyright, trademark or patent laws that exist in any relevant jurisdiction. 
    In addition, in connection with the exercise of these rights, you grant us, and anyone authorized by us, the 
    right to identify you as the author of any of your postings or submissions by name, e-mail address, screen 
    name or user name. You understand that the technical processing and transmission of the Site, including content 
    :submitted by you, may involve transmissions over various networks, and may involve changes to the content to 
    conform and adapt it to technical requirements of connecting networks or devices. You will not receive any 
    compensation for our use of any materials submitted or posted by you.</p>
<p>Linking and Framing. You are permitted to create a hypertext link to our Site so long as the link does not state 
    or imply any sponsorship of your website or service by us. You may not, however, frame or inline link any of 
    the content of the Site, or incorporate into another website or service any of our material, content, or 
    intellectual property, without obtaining our prior written permission.</p>

<!-- SECTION -->
<h3>Site Registration</h3>
<p>To register with us, which may be required to access certain features of the Site, such as message boards, blogs, 
    or photo- and video-sharing pages, you may be asked to create a username and password and to provide us with 
    personally identifiable information such as your name, e-mail address, year of birth, and zip code. You agree 
    to provide true, accurate, current and complete information about yourself as prompted by the registration 
    form. If we have reasonable grounds to suspect that such information is untrue, inaccurate, or incomplete, we 
    have the right to suspend or terminate your account and refuse any and all current or future use of the Site 
    (or any portion thereof). We reserve the right to reject or terminate the use of any username that we deem to 
    be offensive or inappropriate.  We also reserve the right to terminate the use of any username or account, or 
    to deny access to the Site, or any features of the Site, to anyone who violates these Terms of Service.  You 
    are responsible for maintaining the confidentiality of the password and account, and are responsible for all 
    activities (whether by you or by others) that occur under your password or account.  You agree to notify us 
    immediately of any unauthorized use of your password or account or any other breach of security, and to ensure 
    that you exit from your account at the end of each session.  We cannot and will not be liable for any loss or 
    damage arising from your failure to protect your password or account information.  Our use of any information 
    you provide to us as part of the registration process is governed by the terms of our 
    <a href="/privacy-policy">Privacy Policy</a>.</p>

<!-- SECTION -->
<h3>Materials on our Site and Interactive Features</h3>
<p>This Site may include a variety of features, such as message boards, blogs, or photo- and video-sharing pages, 
    that allow users to provide us with content, provide feedback to us and allow users to interact with each 
    other on the Site and post content and materials for display on the Site.</p> 
<p>By accessing and using any such features, you represent and agree: (i) that you are the owner of any material 
    you post or submit, or are making your posting or submission with the express consent of the owner of the material; 
    (ii) that you have the right to grant the rights you grant under these Terms of Service, (iii) that you are 
    making your posting or submission with the express consent of anyone pictured in any material you post or submit, 
    (iv) that you are 13 years of age or older; (v) that the materials will not violate the rights of, or cause 
    injury to, any person or entity; and (vi) that you will indemnify and hold harmless us, our affiliates, our 
    licensees, and each of our and their respective directors, officers, managers, employees, shareholders, agents, 
    representatives and licensors, from and against any liability of any nature arising out of or related to any 
    content or materials displayed on or submitted via the Site by you or by others using your username and password. 
    You also grant us a license to use the materials you post or submit via such features, as described above under 
    the header "Our Rights to Use Content You Submit."</p>
<p>The responsibility for what is posted on message boards, blogs, photo- and video-sharing pages, and other areas 
    on the Site through which users can supply information or material, or sent via any e-mail services that are 
    made available via the Site, lies with each user &mdash; you alone are responsible for the material you post or 
    send. We do not control the messages, information or files that you or others may transmit, post or otherwise 
    provide on or through the Site.</p>
<p>By using the Site, you agree that you will not submit, post or otherwise make available on the Site any content 
    or material that:</p>
<ul>
    <li>is profane, sexually explicit, tortuous, vulgar, obscene, libelous, abusive, infringes the rights of others, 
        interferes with the ability of others to enjoy the Site, or intentionally or unintentionally violates any 
        applicable local, state, national or international law;</li>
    <li>harasses, degrades, intimidates or is hateful toward an individual or group of individuals on the basis of 
        religion, gender, sexual orientation, race, ethnicity, age, or disability;</li>
    <li>harms minors in any way;</li>
    <li>impersonates any person or entity, including, but not limited to, a Site employee, forum moderator or another 
        user, or that falsely  states or otherwise misrepresents your affiliation with a person or  entity;</li>
    <li>includes personal or identifying information about another person without that person's explicit consent;</li>
    <li>is false, deceptive, misleading, or deceitful;</li>
    <li>infringes any copyright, patent, trademark, trade secret or other proprietary rights of any party;</li>
    <li>you do not have a right to make available under any law or under contractual or fiduciary relationships (such 
        as inside information, proprietary and confidential information learned or disclosed as part of employment 
        relationships or under nondisclosure agreements);</li>
    <li>constitutes or includes unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," 
        "chain letters," "pyramid schemes," or any other form of solicitation, except in those areas (such as 
        classifieds) that are designated for such purpose; or</li>
    <li>interferes with or disrupts the Site or servers or networks connected to the Site, disobeys any requirements, 
        procedures, policies or regulations of networks connected to the Site, or contains software viruses or any 
        other computer code, files or programs designed to interrupt, destroy or limit the functionality of the Site 
        or any computer software or hardware or telecommunications equipment;</li>
</ul>

<p>You further agree that you will not collect personal data about, or the email addresses of, other Site users for 
    commercial or unlawful purposes or for purposes of sending unsolicited commercial email, repeatedly post the same 
    or similar content or otherwise impose an unreasonable or disproportionately large load on our infrastructure 
    (including using any bots or other means to scrape or post materials to our site), or take or cause to be taken 
    any action that disrupts the normal flow of postings and dialogue on the Site (such as submitting an excessive 
    number of postings), or that otherwise negatively affects other users' ability to use the Site.</p>
<p>You agree that, in our sole discretion, we may suspend or terminate your password, account or use of the Site, or 
    any part of the Site, and remove and discard any materials that you submit to the Site, at any time, for any reason, 
    without notice. You agree that we will not be liable to you or any third-party for any suspension or termination of 
    your password, account or use of the Site, or any removal of any materials that you have submitted to the Site. In 
    the event that we suspend or terminate your access to and/or use of the Site, you will continue to be bound by the 
    Terms of Service that were in effect as of the date of your suspension or termination.</p>
<p>You acknowledge that we have no obligation to monitor any message boards, blogs, photo- and video-sharing pages, or 
    other areas of the Site through which users can supply information or material. However, we reserve the right, in 
    our sole discretion, to monitor content submitted by users and to modify, edit, delete, and/or refuse to accept any 
    content that in our judgment violates these Terms of Service or is otherwise inappropriate, whether for legal or 
    other reasons.</p>
<p>You further acknowledge and agree that we may preserve content and materials submitted by you, and may also disclose 
    such content and materials if required to do so by law or if, in our business judgment, such preservation or 
    disclosure is reasonably necessary to: (a) comply with legal process; (b) enforce these Terms of Service; (c) respond 
    to claims that any content or materials submitted by you violate the rights of third parties; or (d) protect the 
    rights, property, or personal safety of Site users and/or the public.</p>

<!-- SECTION -->
<h3>Job Listings</h3>
<p>Our Site may permit users to post, and search for, job openings. We do not knowingly accept listings that are not 
    for bona fide job opportunities, that discriminate or intend to discriminate on any illegal basis, or that are 
    otherwise illegal. If you think that an employment listing posted on our Site discriminates on any illegal basis, 
    or is otherwise illegal, please contact us. We encourage you to investigate fully and understand all aspects of 
    any job you are considering.</p>

<!-- SECTION -->
<h3>Site Modifications</h3>
<p>We reserve the right at any time and from time to time to modify or discontinue, temporarily or permanently, the 
    Site, or any portion thereof, with or without notice. You agree that we will not be liable to you or to any 
    third party for any such modification, or discontinuance.</p>

<!-- SECTION -->
<h3>Disclaimers</h3>
<p>Throughout the Site, we have provided links to Internet sites maintained by third parties. Our linking to such 
    sites does not imply an endorsement or sponsorship of such sites, or the information, products or services 
    offered on or through the sites. In addition, we do not operate or control in any respect any information, 
    products or services that third parties may provide on or through the Site or on websites linked to by us on the 
    Site. Please refer to the terms of services on these third-party sites to understand your rights and 
    responsibilities related to the content on, or the content and information your provide to, these sites.</p>
<p>THE INFORMATION, PRODUCTS AND SERVICES OFFERED ON OR THROUGH THE SITE AND ANY THIRD-PARTY SITES ARE PROVIDED 
    "AS IS" AND WITHOUT WARRANTIES OF ANY KIND EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT 
    TO APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES 
    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. WE DO NOT WARRANT THAT THE SITE OR ANY OF ITS FUNCTIONS 
    WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT ANY PART OF THIS SITE, INCLUDING 
    BULLETIN BOARDS, OR THE SERVERS THAT MAKE IT AVAILABLE, ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.</p>
<p>WE DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE SITE OR MATERIALS 
    ON THIS SITE OR ON THIRD-PARTY SITES IN TERMS OF THEIR CORRECTNESS, ACCURACY, TIMELINESS, RELIABILITY OR OTHERWISE.</p>
<p>You are solely responsible for all hardware and/or software necessary to access the Site. You assume the entire 
    cost of and responsibility for any damage to, and all necessary maintenance, repair or correction of, that 
    hardware and/or software.</p>
<p>Your interactions with third parties found on or through our Site, including any purchases, transactions, or 
    other dealings, and any terms, conditions, warranties or representations associated with such dealings, are 
    solely between you and such companies, organizations and/or individuals. You agree that we will not be responsible 
    or liable for any loss or damage of any sort incurred as the result of any such dealings. You also agree that, 
    if there is a dispute between users of this Site, or between a user and any third party, we are under no 
    obligation to become involved, and you agree to release us and our affiliates from any claims, demands and damages 
    of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or 
    in any way related to such dispute and/or our Site.</p>

<!-- SECTION -->
<h3>Limitation of Liability</h3>
<p>UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, WILL WE, OR OUR SUBSIDIARIES, PARENT 
    COMPANIES OR AFFILIATES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES 
    THAT RESULT FROM THE USE OF, OR THE INABILITY TO USE, THIS SITE, INCLUDING ITS MATERIALS, PRODUCTS, OR 
    SERVICES, OR THIRD-PARTY MATERIALS, PRODUCTS, OR SERVICES MADE AVAILABLE THROUGH THIS SITE, EVEN IF WE 
    ARE ADVISED BEFOREHAND OF THE POSSIBILITY OF SUCH DAMAGES. (BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION 
    OR LIMITATION OF CERTAIN CATEGORIES OF DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IN SUCH STATES, 
    OUR LIABILITY AND THE LIABILITY OF OUR SUBSIDIARIES, PARENT COMPANIES AND AFFILIATES, IS LIMITED TO THE 
    FULLEST EXTENT PERMITTED BY SUCH STATE LAW.) YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT WE ARE NOT 
    LIABLE FOR ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF ANY USER. IF YOU ARE DISSATISFIED WITH THE 
    SITE, OR ANY MATERIALS, PRODUCTS, OR SERVICES ON THE SITE, OR WITH ANY OF THE SITE'S TERMS AND CONDITIONS, 
    YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</p>

<!-- SECTION -->
<h3>Indemnification</h3>
<p>You agree to indemnify and hold harmless us, our affiliates, and each of our and their respective directors, 
    officers, managers, employees, shareholders, agents, representatives and licensors, from and against any 
    and all losses, expenses, damages and costs, including reasonable attorneys' fees, that arise out of your 
    use of the Site, violation of these Terms of Service by you or any other person using your account, or 
    your violation of any rights of another. We reserve the right to take over the exclusive defense of any 
    claim for which we are entitled to indemnification under this section. In such event, you agree to provide 
    us with such cooperation as is reasonably requested by us.</p>

<!-- SECTION -->
<h3>Notice of Copyright Infringement</h3>
<p>If you are a copyright owner who believes your copyrighted material has been reproduced, posted or distributed 
    via the Site in a manner that constitutes copyright infringement, please inform our designated copyright agent 
    by sending written notice by U.S. Mail to <em>

    <?php if($copyright_address) : ?>
        <?=$copyright_address?>,
    <?php else : ?>
        VP of Digital Media, 700 St. John Street, Suite 300, Lafayette, LA 70501,
    <?php endif; ?>

</em> or by e-mail to <a href="mailto:<?=$copyright_email?>?subject=<?=$site_name?> Copyright Infringement"><?=$copyright_email?></a>. 
    Please include the following information in your written notice: (1) a detailed description of the copyrighted 
    work that is allegedly infringed upon; (2) a description of the location of the allegedly infringing material 
    on the Site; (3) your contact information, including your address, telephone number, and, if available, e-mail 
    address; (4) a statement by you indicating that you have a good-faith belief that the allegedly infringing 
    use is not authorized by the copyright owner, its agent, or the law; (5) a statement by you, made under penalty 
    of perjury, affirming that the information in your notice is accurate and that you are authorized to act on the 
    copyright owner's behalf; and (6) an electronic or physical signature of the copyright owner or someone 
    authorized on the owner's behalf to assert infringement of copyright and to submit the statement. Please note 
    that the contact information provided in this paragraph is for suspected copyright infringement only. Contact 
    information for other matters is provided elsewhere on the Site.</p>

<!-- SECTION -->
<h3>Miscellaneous</h3>
<p>This agreement constitutes the entire agreement between us and you with respect to the subject matter contained 
    in this agreement and supersedes all previous and contemporaneous agreements, proposals and communications, 
    whether written or oral. You also may be subject to additional terms and conditions that may apply when you 
    use the products or services of a third party that are provided through the Site. In the event of any conflict 
    between any such third-party terms and conditions and these Terms of Service, these Terms of Service will 
    govern. This agreement will be governed by and construed in accordance with the laws of the Louisiana, without 
    giving effect to any principles of conflicts of law. You agree that regardless of any statute or law to the 
    contrary, any claim or cause of action that you may have arising out of or related to use of the Site or these 
    Terms of Service must be filed by you within one year after such claim or cause of action arose or be forever barred.</p>
<p>This agreement is personal to you and you may not assign it. If any provision of this agreement is found to be 
    unlawful, void, or for any reason unenforceable, then that provision will be deemed severable from this 
    agreement and will not affect the validity and enforceability of any remaining provisions. These Terms of 
    Service are not intended to benefit any third party, and do not create any third party beneficiaries. Accordingly, 
    these Terms of Service may only be invoked or enforced by you or us.</p> 
