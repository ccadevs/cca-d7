<h3>For closed captioning concerns regarding <?=$captioning_call_letters?>, please contact our captioning hotline:</h3>

<p>
<?php if ($written_captioning_contact) : ?>
    <b><?=$captioning_contact?></b><br/>
    <?php if ($captioning_contact_title) : ?>
        <?=$captioning_contact_title?><br/>
    <?php endif; ?>
<?php endif; ?>
    <b>Phone:</b> <?=$captioning_phone?><br/>
    <b>Fax:</b> <?=$captioning_fax?><br/>
    <b>Email:</b> <a href="mailto:<?=$captioning_email?>"><?=$captioning_email?></a><br/>
<p>

<p><i>We will make every effort to respond or otherwise resolve your inquiry within 24 hours or 1 business day</i></p>

<h3>Written closed captioning complaints should be directed to the following:</h3>

<p>
<?php if ($written_captioning_contact) : ?>
    <b><?=$written_captioning_contact?></b><br/>
    <?=$written_captioning_contact_title?><br/>
    <?=nl2br($captioning_address)?><br/>
<?php else: ?>
    <b><?=$captioning_contact?></b><br/>
    <?php if ($captioning_contact_title) : ?>
        <?=$captioning_contact_title?><br/>
    <?php endif; ?>
    <?=nl2br($captioning_address)?><br/>
    <b>Phone:</b> <?=$captioning_phone?><br/>
    <b>Fax:</b> <?=$captioning_fax?><br/>
    <b>Toll Free:</b> <?=$captioning_tollfree?><br/>
<?php endif; ?>

<?php if ($written_captioning_contact_email) : ?>
    <b>Email:</b> <a href="mailto:<?=$written_captioning_contact_email?>"><?=$written_captioning_contact_email?></a><br/>
<?php else:?>
    <b>Email:</b> <a href="mailto:<?=$captioning_email?>"><?=$captioning_email?></a><br/>
<?php endif; ?>
</p>

<?php if ($captioning_disclaimer) : ?>
    <div class="messages warning" style="margin-bottom:15px;"><?=$captioning_disclaimer?></div>
<?php endif; ?>

<p><em>Before sending a formal written complaint, we recommend you first contact our captioning hotline. We may be able 
    to resolve your problem immediately without the need for a formal complaint. In any event, we will respond to your 
    complaint within 30 days.</em></p>
