<?php

//  Enabled hook_menu().
// ==================================================================================

function legal_info_menu() {
    $items = array();

    // Content Pages
    $items['terms-of-service'] = array(
        'title' => 'Terms of Service',
        'page callback' => 'theme',
        'page arguments' => array('terms_of_service'),
        'access arguments' => array('access content'),
        'type' => MENU_NORMAL_ITEM,
    );

    $items['privacy-policy'] = array(
        'title' => 'Privacy Policy',
        'page callback' => 'theme',
        'page arguments' => array('privacy_policy'),
        'access arguments' => array('access content'),
        'type' => MENU_NORMAL_ITEM,
    );

    $items['closed-captioning'] = array(
        'title' => 'Closed Captioning Information',
        'page callback' => 'theme',
        'page arguments' => array('closed_captioning'),
        'access arguments' => array('access content'),
        'type' => MENU_NORMAL_ITEM,
    );

    $items['public-file'] = array(
        'title' => 'Public Files',
        'page callback' => 'theme',
        'page arguments' => array('public_file'),
        'access arguments' => array('access content'),
        'type' => MENU_NORMAL_ITEM,
    );

    return $items;
}


//  Enabled hook_system_site_information_settings_alter().
// ==================================================================================

function legal_info_form_system_site_information_settings_alter(&$form, &$form_state) {
    $form['cc_info'] = array(
        '#type' => 'fieldset',
        '#title' => 'Closed Captioning',
        '#collapsible' => true,
        '#collapsed' => true,
        '#weight' => 10,
    );

    $form['cc_info']['captioning_call_letters'] = array(
        '#type' => 'textfield',
        '#title' => 'Station Call Letters',
        '#default_value' => variable_get('captioning_call_letters', ''),
    );

    $form['cc_info']['captioning_email'] = array(
        '#type' => 'textfield',
        '#title' => 'Email',
        '#default_value' => variable_get('captioning_email', ''),
    );

    $form['cc_info']['captioning_address'] = array(
        '#type' => 'textarea',
        '#title' => 'Mailing Address',
        '#default_value' => variable_get('captioning_address', ''),
    );

    $form['cc_info']['captioning_disclaimer'] = array(
        '#type' => 'textarea',
        '#title' => 'Disclaimer',
        '#default_value' => variable_get('captioning_disclaimer', ''),
    );

    $form['cc_info']['default'] = array(
        '#type' => 'fieldset',
        '#collapsible' => true,
        '#collapsed' => false,
        '#title' => 'Closed Captioning Contact Information',
        '#weight' => 3,
    );

    $form['cc_info']['default']['captioning_contact'] = array(
        '#type' => 'textfield',
        '#title' => 'Contact Name',
        '#default_value' => variable_get('captioning_contact', ''),
    );

    $form['cc_info']['default']['captioning_contact_title'] = array(
        '#type' => 'textfield',
        '#title' => 'Contact Title',
        '#default_value' => variable_get('captioning_contact_title', ''),
    );

    $form['cc_info']['default']['captioning_phone'] = array(
        '#type' => 'textfield',
        '#title' => 'Phone',
        '#default_value' => variable_get('captioning_phone', ''),
    );

    $form['cc_info']['default']['captioning_fax'] = array(
        '#type' => 'textfield',
        '#title' => 'Fax',
        '#default_value' => variable_get('captioning_fax', ''),
    );

    $form['cc_info']['default']['captioning_tollfree'] = array(
        '#type' => 'textfield',
        '#title' => 'Toll Free',
        '#default_value' => variable_get('captioning_tollfree', ''),
    );

    $form['cc_info']['written'] = array(
        '#type' => 'fieldset',
        '#collapsible' => true,
        '#collapsed' => true,
        '#title' => 'Closed Captioning Written Contact Information',
        '#weight' => 4,
    );

    $form['cc_info']['written']['written_captioning_contact'] = array(
        '#type' => 'textfield',
        '#title' => 'Contact Name',
        '#default_value' => variable_get('written_captioning_contact', ''),
    );

    $form['cc_info']['written']['written_captioning_contact_title'] = array(
        '#type' => 'textfield',
        '#title' => 'Contact Title',
        '#default_value' => variable_get('written_captioning_contact_title', ''),
    );

    $form['cc_info']['written']['written_captioning_contact_email'] = array(
        '#type' => 'textfield',
        '#title' => 'Email',
        '#default_value' => variable_get('written_captioning_contact_email', ''),
    );

}


//  Enabled hook_theme().
// ==================================================================================

function legal_info_theme($existing, $type, $theme, $path) {
    return array(
        'terms_of_service' => array(
            'template' => 'terms_of_service',
            'variables' => array(
                'station_name' => variable_get('station_name', ''),
                'site_name' => variable_get('station_call_letters', ''),
                'copyright_address' => variable_get('copyright_address', ''),
                'copyright_email' => variable_get('copyright_email', ''),
            ),
        ),

        'privacy_policy' => array(
            'template' => 'privacy_policy',
            'variables' => array(
                'station_name' => variable_get('station_name', ''),
                'site_name' => variable_get('station_call_letters', ''),
                'site_address' => '<span>'.variable_get('station_name', '').'<span><br/>'.variable_get('station_street_address', '').'<br/>'.variable_get('station_city', '').', '.variable_get('station_state', '').' '.variable_get('station_zip', ''),
            ),
        ),

        'closed_captioning' => array(
            'template' => 'closed_captioning',
            'variables' => array(
                'captioning_call_letters' => variable_get('captioning_call_letters', ''),
                'captioning_contact' => variable_get('captioning_contact', ''),
                'captioning_contact_title' => variable_get('captioning_contact_title', ''),
                'written_captioning_contact' => variable_get('written_captioning_contact', ''),
                'written_captioning_contact_title' => variable_get('written_captioning_contact_title', ''),
                'written_captioning_contact_email' => variable_get('written_captioning_contact_email', ''),
                'captioning_disclaimer' => variable_get('captioning_disclaimer', ''),
                'captioning_address' => variable_get('captioning_address', ''),
                'captioning_phone' => variable_get('captioning_phone', ''),
                'captioning_fax' => variable_get('captioning_fax', ''),
                'captioning_tollfree' => variable_get('captioning_tollfree', ''),
                'captioning_email' => variable_get('captioning_email', ''),
            ),
        ),

        'public_file' => array(
            'template' => 'public_file',
        )
    );
}
