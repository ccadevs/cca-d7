<h3>Effective Date: Friday, June 11, 2010</h3>

<p>The following Privacy Policy governs the online information collection practices of <?=$station_name?> 
    ("we" or "us"). Specifically, it outlines the types of information that we gather about you while you 
    are using the <?=$site_name?> website (the "Site"), the information that you may provide to us, and the 
    ways in which we use this information. This Privacy Policy, including our children's privacy statement, 
    does not apply to any information you may provide to us or that we may collect offline and/or through 
    other means (for example, at a live event, via telephone, or through the mail).</p>

<p>Please read this Privacy Policy carefully. By visiting and using the Site, you agree that your use of our 
    Site, and any dispute over privacy, is governed by this Privacy Policy and our Terms of Service. Because 
    the Web is an evolving medium, we may need to change our Privacy Policy at some point in the future, in 
    which case we'll post the changes to this Privacy Policy on this website and update the Effective Date 
    of the policy to reflect the date of the changes. By continuing to use the Site after we post any such 
    changes, you accept the Privacy Policy as modified.</p>

<!-- SECTION -->
<h3>Information That we Collect</h3>
<p>The information that we collect about you while you are using the Site falls into two general categories, 
    as described below. The information we collect may be collected directly by us, or it may be collected by 
    a third-party website hosting provider, or another third-party service provider, on our behalf.</p>

<p><strong>Information That Is Automatically Collected</strong></p>

<p>We collect and store information that is generated automatically, including through web site analytics 
    programs, as you navigate through our Site.  For example, to help make the Site more responsive to the 
    needs of users, we may collect information about your computer's connection to the Internet and employ 
    on our Site, or on certain pages of our Site, a standard feature of browser software called a "cookie" 
    to assign each user a unique, random number that resides on a user's computer.  Cookies are small files 
    that your web browser places on your hard drive for record-keeping purposes. By showing how and when 
    visitors use the Site, cookies help us deliver advertisements, identify how many unique users visit us, 
    and track user trends and patterns. They also prevent you from having to re-enter your preferences on 
    certain areas of the Site where you may have entered preference information before. Of course, you are 
    free to set your Web browser not to accept cookies or delete the cookies that have been placed; however, 
    doing so may render your computer unable to take advantage of the personalized features enjoyed by other 
    users of the Site. The Site also may use web beacons (single-pixel graphic files also known as "transparent 
    GIFs") to access cookies and to count users who visit the Site or open HTML-formatted email messages.</p>

<p><strong>Information Voluntarily Supplied by Website Users</strong></p>

<p>We may collect and store personal or other information (such as your name, email address, year of birth, 
    or zip code) that you voluntarily supply to us online while using the Site (e.g., while on the Site or 
    in responding via email to a feature provided on the Site).  Some examples of this type of information 
    include information that you electronically submit when you contact us with questions, information that 
    you post on blogs, discussion forums or other community posting and social networking areas on our Site, 
    information that you electronically submit when you complete an online registration form to access and 
    use certain features of our Site, and information contained in search requests or questions that you 
    submit to the Site. If you decline to supply or provide us with certain information while using the Site, 
    you may not be able to use or participate in some or all of the features offered through the Site.</p>

<!-- SECTION -->
<h3>How we Use Information We Collect</h3>
<p>We use the information we collect from you while you are using the Site in a variety of ways, including 
    using the information to process your registration request, and customize features and advertising that 
    appear on the Site. Unless you inform us in accordance with the process described below, we reserve the 
    right to use, and to disclose to our affiliates and third-party service providers, all of the information 
    collected from and about you while you are using the Site in any way and for any purpose.</p>

<p>We will not use the information that we collect while you are using the Site, nor will we authorize third 
    parties to use such information, to mail or e-mail promotional offers directly to you unless you have 
    specifically informed us that you would like to receive such promotional offers.</p>

<p>If you do not wish your information to be used for these purposes, you must send a letter to the Online 
    Privacy Coordinator whose address is listed at the end of this Privacy Policy requesting to be taken off 
    any lists of information that may be used for these purposes or that may be given or sold to third-parties.</p>

<p>Our Site also includes links to other websites and provides access to content, products and services 
    offered by third parties, whose privacy policies we do not control. When you access another website or 
    purchase products or services from a third party site, use of any information you provide is governed by 
    the privacy policy of the operator of the site you are visiting or the provider of such products or services.</p>

<p>Be aware that we may occasionally release information about our visitors when release is appropriate to 
    comply with law, to enforce our <a href="/terms-of-service">Term of Service</a>, or to protect the rights, 
    property or safety of users of the Site or the public.</p>

<p>Please also note that as our business grows, we may buy or sell various assets. In the unlikely event that 
    we sell some or all of our assets, or one or more of our websites is acquired by another company, 
    information about our users may be among the transferred assets.</p>

<!-- SECTION -->
<h3>Information Collected By Ad Servers, Third-Party Advertisers and Third-Party Service Providers</h3>
<p>We may use cookies, web beacons and similar technologies, and/or a third-party ad serving software, to collect 
    non-personally identifiable information about Site users and Site activity, and we may use this information to, 
    among other things, serve targeted advertisements on this Site. The information collected allows us to analyze 
    how users use the Site and to track user interests, trends and patterns, thus allowing us to deliver more 
    relevant advertisements to users.</p>

<p>Some of our third-party advertisers and ad servers that place and present advertising on the Site also may 
    collect information from you via cookies, web beacons or similar technologies. These third-party advertisers 
    and ad servers may use the information they collect to help present their advertisements, to help measure and 
    research the advertisements' effectiveness, or for other purposes. The use and collection of your information 
    by these third-party advertisers and ad servers is governed by the relevant third-party's privacy policy and 
    is not covered by our Privacy Policy. Indeed, the privacy policies of these third-party advertisers and ad 
    servers may be different from ours. If you have any concerns about a third party's use of cookies or web 
    beacons or use of your information, you should visit that party's website and review its privacy policy.</p>

<p>The Site also includes links to other websites and provides access to products and services offered by 
    third parties, whose privacy policies we do not control. When you access another website or purchase 
    third-party products or services through the Site, use of any information you provide is governed by 
    the privacy policy of the operator of the site you are visiting or the provider of such products or services.</p>

<p>We also may make content, products and services available through our Site through cooperative relationships 
    with third-party providers, where the brands of our provider partner appear on the Site in connection with 
    such content, products and/or services. We may share with our provider partner any information you provide, 
    or that is collected, in the course of visiting any pages that are made available in cooperation with our 
    provider partner. In some cases, the provider partner may collect information from you directly, in which 
    cases the privacy policy of our provider partner may apply to the provider partner's use of your information. 
    The privacy policy of our provider partners may differ from ours. If you have any questions regarding the 
    privacy policy of one of our provider partners, you should contact the provider partner directly for more information.</p>

<!-- SECTION -->
<h3>Information You Disclose in Public Areas of our Site</h3>
<p>Please keep in mind that whenever you voluntarily make your personal information available for viewing 
    by third parties online - for example on message boards, web logs, through email, or in chat areas - 
    that information can be seen, collected and used by others besides us. We cannot be responsible for 
    any unauthorized third-party use of such information.</p>

<!-- SECTION -->
<h3>Children's Privacy Statement</h3>
<p>This children's privacy statement explains our practices with respect to the online collection and use 
    of personal information from children under the age of thirteen, and provides important information 
    regarding their rights under federal law with respect to such information.</p>

<ul>
    <li>This Site is not directed to children under the age of thirteen and we do NOT knowingly collect 
        personally identifiable information from children under the age of thirteen as part of the Site. 
        We screen users who wish to provide personal information in order to prevent users under the age of 
        thirteen from providing such information. If we become aware that we have inadvertently received 
        personally identifiable information from a user under the age of thirteen as part of the Site, we 
        will delete such information from our records. If we change our practices in the future, we will 
        obtain prior, verifiable parental consent before collecting any personally identifiable information 
        from children under the age of thirteen as part of the Site.</li>
    <li>Because we do not collect any personally identifiable information from children under the age of 
        thirteen as part of the Site, we also do NOT knowingly distribute such information to third parties.</li>
    <li>We do NOT knowingly allow children under the age of thirteen to publicly post or otherwise distribute 
        personally identifiable contact information through the Site.</li>
    <li>Because we do not collect any personally identifiable information from children under the age of 
        thirteen as part of the Site, we do NOT condition the participation of a child under thirteen in 
        the Site's online activities on providing personally identifiable information.</li>
</ul>

<!-- SECTION -->
<h3>Making Changes to Your Information</h3>
<p>If you have provided any personally-identifyable information or subscribed to one or more of our email 
    newsletters, you may change your information, modify your subscriptions, and/or unsubscribe from these 
    newsletters at any time by visiting your profile page.</p>

<!-- SECTION -->
<h3>Questions Regarding Privacy</h3>
<p>If you have any questions about this Privacy Policy, our privacy practices, or your dealings with us, you can contact:</p>

<p class="legal-address"><?=$site_address?></p>
